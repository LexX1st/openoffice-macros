REM  *****  BASIC  *****

sub Main
Dim Doc As Object
Dim StyleFamilies As Object 
dim document   as object
dim dispatcher as object
dim extendedFileName as String

Doc = ThisComponent
StyleFamilies = Doc.StyleFamilies
PageStyles = StyleFamilies.getByName("PageStyles")
DefPage = PageStyles.getByName("Standard")
rem ������������� ������� �������� � ���������� �������� � ������ �������
rem ������ � ������� ����������� � ����������
DefPage.IsLandscape = True
DefPage.Width = 15000
DefPage.Height = 10000
DefPage.LeftMargin = 50
DefPage.RightMargin = 50
DefPage.TopMargin = 50
DefPage.BottomMargin = 50

rem get access to the document

rem MsgBox ConvertFromURL(thisDoc.Url)

If Doc.hasLocation then
    document = ThisComponent.CurrentController.Frame
    dispatcher = createUnoService("com.sun.star.frame.DispatchHelper")
    
    rem �������� ��� ����� ��� ����������
    extendedFileName = ConvertFromURL(Doc.Url)
    parts = Split(extendedFileName, ".")
    extension = parts(Ubound(parts))
    fileNameWithoutExtension = Left(extendedFileName, Len(extendedFileName) - Len(extension)-1)

    rem ������� ��������� ��� ������ ������ ��������
    dim args1(1) as new com.sun.star.beans.PropertyValue
    args1(0).Name = "URL"
    args1(0).Value = ConvertToURL(fileNameWithoutExtension + ".pdf")
    args1(1).Name = "FilterName"
    args1(1).Value = "writer_pdf_Export"

    dispatcher.executeDispatch(document, ".uno:ExportDirectToPDF", "", 0, args1())
Else   
    MsgBox "�������� �� ��������. ��� �������� �������� ������ ���� �������."
EndIf

end sub
